package fr.phenix246.sherlock.parser.ast;

public abstract class AbstractNode implements Node {

    /**
     * Accepts the visitor, calls its visit() method and returns the result.
     * This method just calls {@link #accept(SherlockVisitor, Object)} with
     * <tt>null</tt> as the second argument.
     */
    public <R, A> R accept(SherlockVisitor<R, A> visitor) {
        return accept(visitor, null);
    }
}
