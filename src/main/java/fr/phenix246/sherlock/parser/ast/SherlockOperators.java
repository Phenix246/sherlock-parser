package fr.phenix246.sherlock.parser.ast;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public class SherlockOperators {
    public static final ComparisonOperator EQUAL = new ComparisonOperator("==");
    public static final ComparisonOperator NOT_EQUAL = new ComparisonOperator("!=");
    public static final ComparisonOperator GREATER_THAN = new ComparisonOperator("=gt=", ">");
    public static final ComparisonOperator GREATER_THAN_OR_EQUAL = new ComparisonOperator("=ge=", ">=");
    public static final ComparisonOperator LESS_THAN = new ComparisonOperator("=lt=", "<");
    public static final ComparisonOperator LESS_THAN_OR_EQUAL = new ComparisonOperator("=le=", "<=");
    public static final ComparisonOperator LIKE = new ComparisonOperator("=like=", "=~");
    public static final ComparisonOperator NOT_LIKE = new ComparisonOperator("=nlike=", "!~");
    public static final ComparisonOperator IN = new ComparisonOperator("=in=", "=:",true);
    public static final ComparisonOperator NOT_IN = new ComparisonOperator("=out=", "!:", true);


    public static Set<ComparisonOperator> defaultOperators() {
        return new HashSet<>(asList(EQUAL, NOT_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL,
                LESS_THAN, LESS_THAN_OR_EQUAL, LIKE, IN, NOT_LIKE, NOT_IN));
    }
}
