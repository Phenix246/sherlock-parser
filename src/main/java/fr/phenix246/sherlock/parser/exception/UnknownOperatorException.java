package fr.phenix246.sherlock.parser.exception;

import fr.phenix246.sherlock.parser.ParseException;

/**
 * This exception is thrown when unknown/unsupported comparison operator is parsed.
 */
public class UnknownOperatorException extends ParseException {

    private final String operator;

    public UnknownOperatorException(String operator) {
        this(operator, "Unknown operator: " + operator);
    }

    public UnknownOperatorException(String operator, String message) {
        super(message);
        this.operator = operator;
    }


    public String getOperator() {
        return operator;
    }
}
