package fr.phenix246.sherlock.parser.exception;

/**
 * A top level exception of Sherlock parser that wraps all exceptions occurred in parsing.
 */
public class SherlockParserException extends RuntimeException {
    public SherlockParserException(Throwable cause) {
        super(cause);
    }
}
