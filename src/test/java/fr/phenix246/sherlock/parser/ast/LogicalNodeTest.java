package fr.phenix246.sherlock.parser.ast;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static fr.phenix246.sherlock.parser.ast.SherlockOperators.EQUAL;

import static org.junit.Assert.*;

public abstract class LogicalNodeTest {

    abstract LogicalNode newNode(List children);

    protected Node child1, child2;
    protected LogicalNode node;
    protected List<Node> children;

    @Before
    public void setUp() {
        child1 = new ComparisonNode(EQUAL, "foo", List.of("bar"));
        child2 = new AndNode(new ArrayList<>());
        children = List.of(child1, child2);
        node = newNode(children);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNode1() {
        var it = node.iterator();
        it.next();
        it.remove();
    }

    @Test
    public void testNode2() {
        var node1 = node.withChildren(List.of(child1));
        var node2 = newNode(List.of(child1));
        var node3 = newNode(children);

        assertEquals(node1, node2);
        assertEquals(node, node3);
    }

    @Test
    public void testNode3() {
        children = List.of(child1);
        assertArrayEquals(node.getChildren().toArray(), List.of(child1, child2).toArray());
    }

    public static class AndNodeTest extends LogicalNodeTest {

        LogicalNode newNode(List children) {
            return new AndNode(children);
        }
    }

    public static class OrNodeTest extends LogicalNodeTest {

        LogicalNode newNode(List children) {
            return new OrNode(children);
        }
    }
}
