package fr.phenix246.sherlock.parser.ast;

import fr.phenix246.sherlock.parser.exception.UnknownOperatorException;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

import static fr.phenix246.sherlock.parser.ast.SherlockOperators.*;
import static fr.phenix246.sherlock.parser.ast.LogicalOperator.*;

public class NodesFactoryTest {

    protected final NodesFactory factory = new NodesFactory(Set.of(EQUAL, GREATER_THAN));

    @Test
    public void testCreateLogicalNode() {
        var actual1 = factory.createLogicalNode(AND, List.of());
        assertEquals(actual1.getClass(), AndNode.class);
        var actual2 = factory.createLogicalNode(OR, List.of());
        assertEquals(actual2.getClass(), OrNode.class);
    }

    @Test
    public void testCreateComparisonNode() {
        var opToken = new String[]{"==", "=gt=", ">"};
        var expected = new ComparisonOperator[]{EQUAL, GREATER_THAN, GREATER_THAN};

        for (int i = 0; i < opToken.length; i++) {
            try {
                var node = factory.createComparisonNode(opToken[i], "doctor", List.of("who?"));
                assertEquals(expected[i], node.getOperator());
                assertEquals("doctor", node.getSelectors().get(0));
                assertEquals(List.of("who?"), node.getArguments());
            } catch (UnknownOperatorException e) {
                fail();
            }
        }
    }

    @Test(expected = UnknownOperatorException.class)
    public void testCreateInvalidNode() throws UnknownOperatorException {
        factory.createComparisonNode("=lt=", "sel", List.of("arg"));
    }
}
